<?php
  require("../classes/auth.php");
  require("header.php");
  require("../classes/db.php");
  require("../classes/phpfix.php");
  require("../classes/post.php");

  $sql = "SELECT id, title, text, published FROM posts WHERE id=?";
  $stmt = $mysqli->prepare("SELECT id, title, text, published FROM posts WHERE id=?");
  $stmt->bind_param('i', $_GET['id']);
  if(!$stmt->execute()) echo "Execute failed ";
  $stmt->bind_result($id,$title,$text,$published);
  if($stmt->fetch){
    echo "Debug>result: $id,$title,$text,$published";
    $post = new Post($id,$title,$text,$published);
  }else{
    echo "Debug: SQL Injection?";
  }
  //$post = Post::find($_GET['id']);
  if (isset($_POST['title'])) {
    $post->update($_POST['title'], $_POST['text']);
  }
?>

  <form action="edit.php?id=<?php echo htmlentities($_GET['id']);?>" method="POST" enctype="multipart/form-data">
    Title:
    <input type="text" name="title" value="<?php echo htmlentities($post->title); ?>" /> <br/>
    Text:
      <textarea name="text" cols="80" rows="5">
        <?php echo htmlentities($post->text); ?>
       </textarea><br/>

    <input type="submit" name="Update" value="Update">

  </form>

<?php
  require("footer.php");

?>
