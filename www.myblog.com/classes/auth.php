<?php
  //void session_set_cookie_params (int $lifetime[, string $path[, string $domain[, bool $secure = false[, bool $httponly = false]]]])
  ini_set('session.gc_maxlifetime', 900);
  session_set_cookie_params(900); //set for 15 mins
  session_start();
  require('../classes/db.php');
  require('../classes/user.php');

  if (isset($_POST["user"]) and isset($_POST["password"]) )
    if (User::login($_POST["user"],$_POST["password"])){
      $_SESSION["admin"] = User::SITE;
      $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
      $_SESSION["ip"] = $_SERVER["REMOTE_ADDRESS"];
    }


  if (!isset($_SESSION["admin"] ) or $_SESSION["admin"] != User::SITE) {
    header( 'Location: /admin/login.php' ) ;
    die();
  }

  if($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
    echo "Session hijacking detected!";
    die();
  }

  if($_SESSION["ip"] != $_SERVER["REMOTE_ADDRESS"]){
    echo "Session hijacking detected!";
    die();
  }

?>
