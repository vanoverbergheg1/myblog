<?php
require "db.php";

class Post{
  public $id, $title, $text, $published;
  function __construct($id, $title, $text, $published){
    $this->title= htmlspecialchars($title);
    $this->text = htmlspecialchars($text);
    $this->published= htmlspecialchars($published);
    $this->id = intval($id);
  }


  function all($cat=NULL,$order =NULL) {
    $sql = "SELECT * FROM posts";
    if (isset($order))
      $sql .= "order by ".$mysqli->real_escape_string($order);
    $results= $mysqli->query($results);
    echo $results;
    $posts = Array();
    if ($results) {
      echo "Works";
      while ($row = $results->fetch_assoc()) {
        $posts[] = new Post($row['id'],$row['title'],$row['text'],$row['published']);
      }
    }
    else {
      echo mysql_error();
    }
    global $mysqli;
    $sql = "SELECT * FROM posts";
    if (isset($order))
      $sql .= "order by ".$mysqli->real_escape_string($order);
    $stmt = $mysqli->prepare($sql);
    if(!$stmt->execute()) echo "Execute Failed";
    $stmt->bind_result($id,$title,$test,$published);
    $posts = Array();
    while($stmt->fetch()){
      $posts[] = new Post(h($id),h($title),h($test),h($published));
    }

    return $posts;
  }


  function render_all($pics) {
    echo "<ul>\n";
    foreach ($pics as $pic) {
      echo "\t<li>".$pic->render()."</a></li>\n";
    }
    echo "</ul>\n";
  }
 function render_edit() {
    $str = "<img src=\"uploads/".h($this->img)."\" alt=\"".h($this->title)."\" />";
    return $str;
  }


  function render() {
    $str = "<h2 class=\"title\"><a href=\"/post.php?id=".h($this->id)."\">".h($this->title)."</a></h2>";
    $str.= '<div class="inner" align="center">';
    $str.= "<p>".htmlentities($this->text)."</p></div>";
    $str.= "<p><a href=\"/post.php?id=".h($this->id)."\">";
    $count = $this->get_comments_count();
    switch ($count) {
    case 0:
        $str.= "Be the first to comment";
        break;
    case 1:
        $str.= "1 comment";
        break;
    case 2:
        $str.= $count." comments";
        break;
    }
    $str.= "</a></p>";
    return $str;
  }
  function add_comment() {
    global $mysqli;
    $sql  = "INSERT INTO comments (title,author, text, post_id) values ('";
    $sql .= $mysqli->real_escape_string(htmlspecialchars($_POST["title"]))."','";
    $sql .= $mysqli->real_escape_string(htmlspecialchars($_POST["author"]))."','";
    $sql .= $mysqli->real_escape_string(htmlspecialchars($_POST["text"]))."',";
    $sql .= intval($this->id).")";
    $result = $mysqli->query($sql);
    echo mysqli_error();
  }
  function render_with_comments() {
    $str = "<h2 class=\"title\"><a href=\"/post.php?id=".h($this->id)."\">".h($this->title)."</a></h2>";
    $str.= '<div class="inner" style="padding-left: 40px;">';
    $str.= "<p>".htmlentities($this->text)."</p></div>";
    $str.= "\n\n<div class='comments'><h3>Comments: </h3>\n<ul>";
    foreach ($this->get_comments() as $comment) {
      $str.= "\n\t<li>".$comment->text."</li>";
    }
    $str.= "\n</ul></div>";
    return $str;
  }

  function get_comments_count() {
    if (!preg_match('/^[0-9]+$/', $this->id)) {
      die("ERROR: INTEGER REQUIRED");
    }
    $comments = Array();
    /*$result = mysql_query("SELECT count(*) as count FROM comments where post_id=".$this->id);
    $row = mysql_fetch_assoc($result);
    return $row['count'];*/
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT count(*) as count FROM comments where post_id=?");
    $stmt->bind_param("i", $this->id);
    if(!$stmt->execute()) echo "Execute Failed";
    $stmt->bind_result($count);
    $stmt->fetch();
    return $count;
  }

  function get_comments() {
    global $mysqli;
    if (!preg_match('/^[0-9]+$/', $this->id)) {
      die("ERROR: INTEGER REQUIRED");
    }
    $comments = Array();
    $results = $mysqli->query("SELECT * FROM comments where post_id=".$this->id);
    if (isset($results)){
      while ($row = $results->fetch_assoc($results)) {
        $comments[] = Comment::from_row($row);
      }
    }
    return $comments;
  }

  function find($id) {
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT * FROM posts where id=?");
    $stmt->bind_param("i", $id);
    if(!$stmt->execute()) echo "Execute Failed";
    $stmt->bind_result($id,$title,$test,$published);
    if($stmt->fetch()){
        $post = new Post(h($id), h($title), h($text), $h($published));
    }
    /*$result = mysql_query("SELECT * FROM posts where id=".$id);
    $row = mysql_fetch_assoc($result);
    if (isset($row)){
      $post = new Post($row['id'],$row['title'],$row['text'],$row['published']);
    }*/
    return $post;

  }
  function delete($id) {
    global $mysqli;
    if (!preg_match('/^[0-9]+$/', $id)) {
      die("ERROR: INTEGER REQUIRED");
    }
    $result = $mysqli->query("DELETE FROM posts where id=".(int)$id);
  }

  function update($title, $text) {
      global $mysqli;
      $sql = "UPDATE posts SET title='";
      $sql .= $mysqli->real_escape_string(htmlspecialchars($_POST["title"]))."',text='";
      $sql .= $mysqli->real_escape_string( htmlspecialchars($_POST["text"]))."' WHERE id=";
      $sql .= intval($this->id);
      $result = $mysqli->query($sql);
      $this->title = $title;
      $this->text = $text;
  }

  function create(){
      global $mysqli;
      $sql = "INSERT INTO posts (title, text) VALUES ('";
      $title = $mysqli->real_escape_string(htmlentities($_POST["title"]));
      $text = $mysqli->real_escape_string( htmlentities($_POST["text"]));
      $sql .= $title."','".$text;
      $sql.= "')";
      $result = $mysqli->query($sql);

  }
}
?>
